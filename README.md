# hello-aws

API service that returns runtime AWS account ID.  
How it works:  
    On startup, the service parses the AWS account ID from the ECS container metadata API and returns it on client request (GET /).

## Usage:
```bash
curl https://awsid-api.murodov.org
```

response example:
```json
{
    "AWS Account ID": "000000000000"
}
```


## Deploy:

#### 1. Create Fargate cluster with terraform
  1. Prepare
    ```bash
    cd terraform
    terraform init
    export AWS_ACCESS_KEY_ID=XXXXXXXXXXXXX
    export AWS_SECRET_ACCESS_KEY=XXXXXXXXX
    export AWS_DEFAULT_REGION=eu-central-1
    ```
  2. Change the custom domain name in variable.tf
  3. deploy cluster
    ```bash
    terraform apply
    ```
  4. add DNS CNAME record for custom_domain with ALB domain
  5. add DNS record for validation (domain_validations output)
  6. wait a few minutes to issue the certificate and apply again
    ```bash
    terraform apply
    ```
  7. (Optional) uncomment redirect to HTTPS rule in ecs.tf


#### 2. Build and deploy the container to AWS Fargate using Gitlab CI

CI VARS:
| KEY                        | VALUE                             | Description                                                      |
|----------------------------|-----------------------------------|------------------------------------------------------------------|
| AWS_ACCESS_KEY_ID          | Access key ID of the deployer     | For authenticating AWS CLI.                                      |
| AWS_SECRET_ACCESS_KEY      | Secret access key of the deployer | For authenticating AWS CLI.                                      |
| AWS_DEFAULT_REGION         | eu-central-1                      | For authenticating AWS CLI.                                      |
| CI_AWS_ECS_CLUSTER         | awsid-api                         | The ECS cluster is accessed by production_ecs job.               |
| CI_AWS_ECS_SERVICE         | awsid-api                         | The ECS service of the cluster is updated by production_ecs job. |
| CI_AWS_ECS_TASK_DEFINITION | awsid-api                         | The ECS task definition is updated by production_ecs job.        |


References:  
- https://docs.gitlab.com/ee/ci/cloud_deployment/ecs/quick_start_guide.html
- https://section411.com/2019/07/hello-world
- https://docs.aws.amazon.com/AmazonECS/latest/userguide/task-metadata-endpoint-v4-fargate.html
