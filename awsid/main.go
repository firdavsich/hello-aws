package main

import (
	"encoding/json"
	"fmt"
	"io/ioutil"
	"log"
	"net/http"
	"os"
	"strings"
)

type taskStruct struct {
	TaskARN string `json:"TaskARN"`
}

func main() {

	task := taskStruct{}

	res, err := http.Get(os.Getenv("ECS_CONTAINER_METADATA_URI_V4") + "/task")
	if err != nil {
		log.Fatal(err)
	}
	defer res.Body.Close()

	body, err := ioutil.ReadAll(res.Body)
	if err != nil {
		log.Fatal(err)
	}
	json.Unmarshal(body, &task)

	awsID := strings.Split(task.TaskARN, ":")[4]
	data := []byte(fmt.Sprintf(`{"AWS Account ID": "%s"}`, awsID))

	if err := ioutil.WriteFile("/json/awsid.json", data, 0644); err != nil {
		log.Fatal(err)
	}
	log.Println("AWS Account ID written to /json/awsid.json")
}
