FROM golang:alpine as builder
COPY awsid /app
WORKDIR /app
RUN CGO_ENABLED=0 go build -ldflags '-extldflags "-static"' -o awsid


FROM nginx:alpine as runner
COPY --from=builder /app/awsid /awsid
COPY nginx.conf /etc/nginx/conf.d/default.conf
COPY entrypoint.sh /entrypoint.sh
WORKDIR /json

ENTRYPOINT [ "/entrypoint.sh" ]