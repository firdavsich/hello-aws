resource "aws_ecs_cluster" "awsid_api" {
  name = "awsid-api"
}

resource "aws_ecs_service" "awsid_api" {
  name            = "awsid-api"
  task_definition = aws_ecs_task_definition.awsid_api.arn
  cluster         = aws_ecs_cluster.awsid_api.id
  launch_type     = "FARGATE"
  network_configuration {
    assign_public_ip = false

    security_groups = [
      aws_security_group.egress_all.id,
      aws_security_group.ingress_api.id,
    ]

    subnets = [
      aws_subnet.private_a.id,
      aws_subnet.private_b.id,
    ]
  }
  load_balancer {
    target_group_arn = aws_lb_target_group.awsid_api.arn
    container_name   = "awsid-api"
    container_port   = "80"
  }
  desired_count = 1

}

resource "aws_cloudwatch_log_group" "awsid_api" {
  name = "/ecs/awsid-api"
}

resource "aws_ecs_task_definition" "awsid_api" {
  family = "awsid-api"

  container_definitions = <<EOF
  [
    {
      "name": "awsid-api",
      "image": "registry.gitlab.com/firdavsich/awsid-api/main:latest",
      "portMappings": [
        {
          "containerPort": 80
        }
      ],
      "logConfiguration": {
        "logDriver": "awslogs",
        "options": {
          "awslogs-region": "eu-central-1",
          "awslogs-group": "/ecs/awsid-api",
          "awslogs-stream-prefix": "ecs"
        }
      }
    }
  ]
  EOF

  execution_role_arn = aws_iam_role.awsid_api_task_execution_role.arn

  cpu                      = 256
  memory                   = 512
  requires_compatibilities = ["FARGATE"]

  network_mode = "awsvpc"
}

resource "aws_iam_role" "awsid_api_task_execution_role" {
  name               = "awsid-api-task-execution-role"
  assume_role_policy = data.aws_iam_policy_document.ecs_task_assume_role.json
}

data "aws_iam_policy_document" "ecs_task_assume_role" {
  statement {
    actions = ["sts:AssumeRole"]

    principals {
      type        = "Service"
      identifiers = ["ecs-tasks.amazonaws.com"]
    }
  }
}


data "aws_iam_policy" "ecs_task_execution_role" {
  arn = "arn:aws:iam::aws:policy/service-role/AmazonECSTaskExecutionRolePolicy"
}

resource "aws_iam_role_policy_attachment" "ecs_task_execution_role" {
  role       = aws_iam_role.awsid_api_task_execution_role.name
  policy_arn = data.aws_iam_policy.ecs_task_execution_role.arn
}


resource "aws_lb_target_group" "awsid_api" {
  name        = "awsid-api"
  port        = 80
  protocol    = "HTTP"
  target_type = "ip"
  vpc_id      = aws_vpc.awsid_vpc.id

  #   health_check {
  #     enabled = true
  #     path    = "/health"
  #   }

  depends_on = [aws_alb.awsid_api]
}

resource "aws_alb" "awsid_api" {
  name               = "awsid-api-lb"
  internal           = false
  load_balancer_type = "application"

  subnets = [
    aws_subnet.public_a.id,
    aws_subnet.public_b.id,
  ]

  security_groups = [
    aws_security_group.http.id,
    aws_security_group.https.id,
    aws_security_group.egress_all.id,
  ]

  depends_on = [aws_internet_gateway.igw]
}

resource "aws_alb_listener" "awsid_api_http" {
  load_balancer_arn = aws_alb.awsid_api.arn
  port              = "80"
  protocol          = "HTTP"

  default_action {
    type             = "forward"
    target_group_arn = aws_lb_target_group.awsid_api.arn
  }
  # default_action {
  #   type = "redirect"

  #   redirect {
  #     port        = "443"
  #     protocol    = "HTTPS"
  #     status_code = "HTTP_301"
  #   }
  # }
}

output "alb_url" {
  value = "http://${aws_alb.awsid_api.dns_name}"
}


resource "aws_acm_certificate" "awsid_api" {
  domain_name       = var.domain_name
  validation_method = "DNS"
}

output "domain_validations" {
  value = aws_acm_certificate.awsid_api.domain_validation_options
}

output "cert_status" {
  value = aws_acm_certificate.awsid_api.status
}

resource "aws_alb_listener" "awsid_api_https" {
  load_balancer_arn = aws_alb.awsid_api.arn
  port              = "443"
  protocol          = "HTTPS"
  certificate_arn   = aws_acm_certificate.awsid_api.arn

  default_action {
    type             = "forward"
    target_group_arn = aws_lb_target_group.awsid_api.arn
  }
  count = aws_acm_certificate.awsid_api.status == "ISSUED" ? 1 : 0
}
